<?php

use App\Admin\Controllers\ProductsController;
use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    $router->resource('/products', 'ProductsController');
    $router->resource('/category', CategoryController::class);
    $router->get('/sync-products', [ProductsController::class, 'sync']);
});
