<?php

namespace App\Admin\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Utils\Facades\Crm;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;

class ProductsController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Product';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Product());

        $grid->column('id', __('Id'));
        $grid->column('name', __('Name'));
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        $grid->categories()->display(function ($roles) {

            $roles = array_map(function ($role) {
                return "<span class='label label-success'>{$role['name']}</span>";
            }, $roles);

            return join('&nbsp;', $roles);
        });

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Product::findOrFail($id));



        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Product());
        $form->text('name', __('Name'))->rules('required|min:3');
        $form->textarea('description', __('Description'))->rules('required|min:3');
        $form->number('price', __('Price'))->default(0)->rules('required|min:0');
        $form->image('image', __('Image'))->rules('required|image');
        $form->multipleSelect('categories','Categories')->options(Category::all()->pluck('name','id'));

        return $form;
    }

    protected function sync(){
        dd(Crm::getProducts());
    }
}
