<?php


namespace App\Utils\Facades;


use Illuminate\Support\Facades\Facade;

/**`
 * Class CurrencyHelper
 * @method static getProducts()
 * @package App\Utils\Facades
 */
class Crm extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \App\Utils\Crm::class;
    }
}
