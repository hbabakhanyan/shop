<?php


namespace App\Utils;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;

class Crm
{

    /**
     * @var mixed
     */
    public static $bearer;
    public static $http;

    public function __construct()
    {

        self::$http = Http::baseUrl(config('admin.crm.url'))->acceptJson();
        if (!self::$bearer) {
            self::$bearer = Cache::remember('shop-barer', 3600, function () {
                $response = self::$http->post('api/login', [
                    'email' => config('admin.crm.email'),
                    'password' => config('admin.crm.password')
                ])->json();

                return $response['token'];
            });

        }
        self::$http->withToken(self::$bearer);
    }

    public function getProducts()
    {
        return self::$http->get('api/items/index')->json();
    }
}
