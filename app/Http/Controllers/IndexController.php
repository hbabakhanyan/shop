<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Utils\Facades\Crm;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {

        $products = Product::query()->when($request->has('search') && $request->get('search'), function ($q) use ($request){
            $q->where('name', 'LIKE', "%{$request->get('search')}%");
        })->get();
        $categories = Category::query()->get();

        return view('products', compact('products', 'categories'));

    }
    public function category(Category $category, Request $request)
    {
        $products = $category->products()->when($request->has('search') && $request->get('search'), function ($q) use ($request){
            $q->where('name', 'LIKE', "%{$request->get('search')}%");
        })->get();
        $categories = Category::query()->get();

        return view('products', compact('products', 'categories'));

    }
    public function product(Product $product, Request $request)
    {
        $categories = Category::query()->get();
        $products = Product::query()->when($request->has('search') && $request->get('search'), function ($q) use ($request){
            $q->where('name', 'LIKE', "%{$request->get('search')}%");
        })->limit(5)->inRandomOrder()->get();

        return view('product', compact('product', 'products', 'categories'));

    }
}
