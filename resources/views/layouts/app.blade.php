<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{mix('/css/app.css')}}">
</head>
<body class="antialiased" x-data="{cartOpen: false, isOpen: false, cartProducts: []}">
@include('layouts/header')
@yield('content')
@include('layouts/footer')
<script src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js" defer></script>
<script>

    document.addEventListener('alpine:init', () => {
        Alpine.store('cartProducts', {
            products: JSON.parse(localStorage.getItem('products'))??[],
            syncStorage() {
                localStorage.setItem('products', JSON.stringify(this.products))
            },
            addToCart(product) {
                let push = true;
                this.products.forEach(item => {
                    if (item.id === product.id) {
                        item.count = product.count;
                        push = false;
                    }
                })
                if( push ) {
                    this.products.push({...product});
                }
                localStorage.setItem('products', JSON.stringify(this.products))
            }
        })
    })
</script>
</body>
</html>
